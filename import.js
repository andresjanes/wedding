/* eslint-disable */
require('dotenv').config()
const firebase = require('firebase/app')
require('firebase/firestore')

const config = {
    apiKey: process.env.VUE_APP_FIREBASE_API_KEY,
    authDomain: process.env.VUE_APP_FIREBASE_AUTH_DOMAIN,
    databaseURL: process.env.VUE_APP_FIREBASE_DATABASE_URL,
    projectId: process.env.VUE_APP_FIREBASE_PROJECT_ID,
    storageBucket: process.env.VUE_APP_FIREBASE_STORAGE_BUCKET,
    messagingSenderId: process.env.VUE_APP_FIREBASE_MESSAGING_SENDER_ID,
    appId: process.env.VUE_APP_FIREBASE_APP_ID,
    measurementId: process.env.VUE_APP_FIREBASE_MEASUREMENT_ID
}

firebase.initializeApp(config)

const db = firebase.firestore()

if (process.env.NODE_ENV !== 'production') {
    db.useEmulator('localhost', 9090)
}

const csv = require('csv-parser')
const fs = require('fs')
const results = []

fs.createReadStream('guests.csv')
    .pipe(csv())
    .on('data', (data) => {
        results.push(data)

        db.collection('guests').add({
            ...data,
            attending: 0,
            online_only: parseInt(data.online_only),
            guests: parseInt(data.guests)
        })
    })
    .on('end', () => {
        console.log(results)
    })
