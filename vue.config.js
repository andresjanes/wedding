const {APP_TITLE} = require('./src/store/static/app')
const ImageminPlugin = require('imagemin-webpack-plugin').default
const ImageminMozJpeg = require('imagemin-mozjpeg')

let plugins = []

if (process.env.NODE_ENV === 'production') {
    plugins.push(new ImageminPlugin({
        plugins: [
            ImageminMozJpeg({
                quality: 50,
                progressive: true
            })
        ]
    }))
}

module.exports = {
    runtimeCompiler: true,
    transpileDependencies: ['vuetify'],
    configureWebpack: {plugins},

    pwa: {
        name: APP_TITLE,
        themeColor: '#9cb9d1',
        msTileColor: '#9cb9d1',
        workboxPluginMode: 'InjectManifest',
        workboxOptions: {
            swSrc: 'src/service-worker.js',
            exclude: [/\.map$/, /manifest\.json$/]
        }
    },

    pluginOptions: {
        i18n: {
            locale: 'en',
            fallbackLocale: 'en',
            localeDir: 'locales',
            enableInSFC: true
        }
    }
}
