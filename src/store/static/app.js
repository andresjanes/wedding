const APP_TITLE = 'Xiomi & Santi'
const APP_FOOTER = 'Xiomi & Santi 2021'

module.exports = {
    APP_TITLE,
    APP_FOOTER
}
