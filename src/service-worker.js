/* eslint-disable, no-undef, no-restricted-globals, no-console */
/* global workbox */

// update service worker, after user has requested to 'Refresh' via snackbar on UI
self.addEventListener('message', (event) => {
    if (event.data && event.data.type === 'SKIP_WAITING') {
        self.skipWaiting()
    }
})

// Start controlling any existing clients as soon as it activates
workbox.core.clientsClaim()

// Config for workbox precaching.  See https://goo.gl/S9QRab
workbox.precaching.cleanupOutdatedCaches()
self.__precacheManifest = [].concat(self.__precacheManifest || [])
workbox.precaching.precacheAndRoute(self.__precacheManifest, {})
