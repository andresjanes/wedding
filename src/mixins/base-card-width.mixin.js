export default {
    computed: {
        cardWidth () {
            return this.$vuetify.breakpoint.mobile
                ? '100vw'
                : '80vw'
        }
    }
}
